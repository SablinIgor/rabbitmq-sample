# General

Тестовые скрипты для работы с RedditMQ.

Демонстрация отправки сообщения в очередь и чтение из нее.

# Install

<pre>
git clone https://gitlab.com/SablinIgor/rabbitmq-sample.git

cd rabbitmq-sample

pip3 install -r requirements.txt
</pre>

# Start RabbitMQ in docker (if you need it)

<pre>
docker run -d -h [YOUR_HOSTNAME]                                    \
           --name rabbit                                            \
           -p "4369:4369"                                           \
           -p "5672:5672"                                           \
           -p "15672:15672"                                         \
           -p "25672:25672"                                         \
           -p "35197:35197"                                         \
           -e "RABBITMQ_USE_LONGNAME=true"                          \
           -e "RABBITMQ_LOGS=/var/log/rabbitmq/rabbit.log"          \
           -v /data:/var/lib/rabbitmq \
           -v /data/logs:/var/log/rabbitmq \
           rabbitmq:3.6.6-management
</pre>

# Config

Поменяйте в скриптах имя хоста (параметр host) с RabbitMQ на актуальное

<pre>
connection = pika.BlockingConnection(pika.ConnectionParameters(host='node-1.sablin.de'))
</pre>

# Send message 

<pre>
$ python3 sample_send.py 
Type something to test this out: My message!
Send 'My message!'
</pre>

# Receive message

<pre>
$ python3 sample_recv.py 
Waiting to get messages from sender. To exit press CTRL+C
Received message from sender: b'My message!'
</pre>
