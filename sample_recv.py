import pika

# connect to the RabbitMQ server (!!! use YOUR host !!!)
connection = pika.BlockingConnection(pika.ConnectionParameters(host='node-1.sablin.de'))
channel = connection.channel()

# declare the queue in case it doesn't already exists
channel.queue_declare(queue='testqueue')

# callback function waiting messages
def callbackfunc(ch, method, properties, text):
    print("Received message from sender: %r" % text)

channel.basic_consume(
    'testqueue',
    callbackfunc,
    auto_ack=True
    )

print('Waiting to get messages from sender. To exit press CTRL+C')
channel.start_consuming()
