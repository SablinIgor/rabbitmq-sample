#!/usr/bin/env python

import pika

txt = input("Type something to sent: ")

# connect to the RabbitMQ server (!!! use YOUR host !!!)
connection = pika.BlockingConnection(pika.ConnectionParameters(host='node-1.sablin.de'))
channel = connection.channel()

# declare the queue in case it doesn't already exists
channel.queue_declare(queue='testqueue')

# add a message to the queue
channel.basic_publish(
    exchange='',
    routing_key='testqueue',
    body=txt
)

print("Send %r" % txt)

# close the queue connection
connection.close()
